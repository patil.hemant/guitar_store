# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


a1 = Accessory.create(name: 'guitar pick' , price: 100, accessory_type: 'wooden')
a2 = Accessory.create(name: 'guitar pick' , price: 50, accessory_type: 'plastic')
a3 = Accessory.create(name: 'guitar strap' , price: 200, accessory_type: 'regular')
a4 = Accessory.create(name: 'guitar strap' , price: 150, accessory_type: 'large')
a5 = Accessory.create(name: 'guitar strap' , price: 100, accessory_type: 'extra large')

g1 = Guitar.create(brand_name: 'yamaha',	guitar_type: 'acoustic', price: 2000,	no_of_strings: 5)
g1.guitar_accessories.create(:accessory_id => a1.id)
g1.guitar_accessories.create(:accessory_id => a4.id)

g2 = Guitar.create(brand_name: 'yamaha',	guitar_type: 'acoustic', price: 4000,	no_of_strings: 5)
g2.guitar_accessories.create(:accessory_id => a1.id)
g2.guitar_accessories.create(:accessory_id => a5.id)

g3 = Guitar.create(brand_name: 'yamaha',	guitar_type: 'acoustic', price: 4000,	no_of_strings: 7)
g3.guitar_accessories.create(:accessory_id => a1.id)
g3.guitar_accessories.create(:accessory_id => a4.id)

g4 = Guitar.create(brand_name: 'yamaha',	guitar_type: 'acoustic', price: 4000,	no_of_strings: 7)
g4.guitar_accessories.create(:accessory_id => a1.id)
g4.guitar_accessories.create(:accessory_id => a5.id)

g5 = Guitar.create(brand_name: 'yamaha',	guitar_type: 'acoustic', price: 2000,	no_of_strings: 7)
g5.guitar_accessories.create(:accessory_id => a1.id)
g5.guitar_accessories.create(:accessory_id => a5.id)

g6 = Guitar.create(brand_name: 'epiphone', guitar_type: 'acoustic', price: 2000,	no_of_strings: 7)
g6.guitar_accessories.create(:accessory_id => a1.id)
g6.guitar_accessories.create(:accessory_id => a4.id)

g7 = Guitar.create(brand_name: 'epiphone', guitar_type: 'electric', price: 2000, no_of_strings: 5)
g7.guitar_accessories.create(:accessory_id => a2.id)
g7.guitar_accessories.create(:accessory_id => a4.id)

g8 = Guitar.create(brand_name: 'epiphone', guitar_type: 'electric', price: 2000, no_of_strings: 5)
g8.guitar_accessories.create(:accessory_id => a2.id)
g8.guitar_accessories.create(:accessory_id => a5.id)

g9 = Guitar.create(brand_name: 'epiphone', guitar_type: 'electric', price: 2000, no_of_strings: 4)
g9.guitar_accessories.create(:accessory_id => a2.id)
g9.guitar_accessories.create(:accessory_id => a4.id)

g10 = Guitar.create(brand_name: 'jackson', guitar_type: 'electric', price: 3000, no_of_strings: 4)
g10.guitar_accessories.create(:accessory_id => a2.id)

g11 = Guitar.create(brand_name: 'jackson', guitar_type: 'electric', price: 3000, no_of_strings: 4)
g11.guitar_accessories.create(:accessory_id => a2.id)

g12 = Guitar.create(brand_name: 'jackson', guitar_type: 'electric', price: 3000, no_of_strings: 4)
g12.guitar_accessories.create(:accessory_id => a2.id)

g13 = Guitar.create(brand_name: 'jackson', guitar_type: 'electric', price: 3000, no_of_strings: 5)
g13.guitar_accessories.create(:accessory_id => a2.id)