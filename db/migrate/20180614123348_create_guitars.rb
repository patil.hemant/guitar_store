class CreateGuitars < ActiveRecord::Migration[5.1]
  def change
    create_table :guitars do |t|
      t.string :brand_name
      t.string :guitar_type
      t.float :price
      t.integer :no_of_strings
      t.string :uniq_serial_number
      t.date :bought_date

      t.timestamps
    end
  end
end
