class CreateGuitarAccessories < ActiveRecord::Migration[5.1]
  def change
    create_table :guitar_accessories do |t|
      t.integer :guitar_id
      t.integer :accessory_id

      t.timestamps
    end
    add_index :guitar_accessories, :guitar_id
    add_index :guitar_accessories, :accessory_id
  end
end
