class Accessory < ApplicationRecord
	has_many :guitars, through: :guitar_accessories
	has_many :guitars
end
