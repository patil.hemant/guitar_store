class Guitar < ApplicationRecord
	has_many :accessories, through: :guitar_accessories
	has_many :guitar_accessories
end
