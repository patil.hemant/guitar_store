class GuitarAccessory < ApplicationRecord
	belongs_to :guitar
	belongs_to :accessory
end
